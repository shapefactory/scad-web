var path = require('path'),
    rootPath = path.normalize(__dirname + '/..'),
    env = process.env.NODE_ENV || 'development';

var config = {
  development: {
    root: rootPath,
    api_root: '/api/v1/',
    middleware: ['cqrs', 'authentication', 'localuser'],
    session_key: '7fe771be5464cdf8e51a66573ce07c0c81c157a9e2346d75051d3583c479b2e2',
    app: {
      name: 'scad-web'
    },
    port: 3000,
    db: 'mongodb://localhost/scad-dev',
    security: {
        allowedDomains: '*',
    }

  },

  test: {
    root: rootPath,
    api_root: '/api/v1/',
    middleware: ['cqrs', 'authentication', 'localuser'],
    session_key: 'secret',
    app: {
      name: 'scad-web'
    },
    port: 3000,
    db: 'mongodb://localhost/scad-test',
    security: {
        allowedDomains: '*',
    }
  },

  production: {
    root: rootPath,
    api_root: '/api/v1/',
    middleware: ['cqrs', 'authentication', 'localuser'],
    session_key: 'a26a42e0161cc0721770b77a537064cc31d2aea16aaa502f1a1a157abea8e5cf',
    app: {
      name: 'scad-web'
    },
    port: 3000,
    db: 'mongodb://localhost/scad',
    security: {
        allowedDomains: '*',
    }
  }
};

module.exports = config[env];
