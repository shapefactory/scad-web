'use strict';

var express = require('express');
var glob = require('glob');
var path = require('path');

var favicon = require('serve-favicon');
var logger = require('morgan');
var cookieParser = require('cookie-parser');

var bodyParser = require('body-parser');
var compress = require('compression');
var methodOverride = require('method-override');
var flash = require('connect-flash');
var morgan       = require('morgan');

var session = require('cookie-session');
//var session = require('express-session');
var passport = require('./passport');

module.exports = function(app, config) {
    app.set('views', config.root + '/app/views');
    app.set('view engine', 'ejs');

    app.use(favicon(config.root + '/public/img/favicon.ico'));
    app.use(logger('dev'));
    app.use(bodyParser.json());
    app.use(bodyParser.urlencoded({
        extended: true
    }));
    app.use(cookieParser());
    app.use(compress());
    app.use(express.static(config.root + '/public'));
    app.use(methodOverride());


    app.use(session({ name: config.app_name || 'scad-session', secret: config.session_key}));
    app.use(flash());

    app.use(passport.initialize());
    app.use(passport.session());

    var middlewares = glob.sync(config.root + '/app/middleware/*.js');
    var middlewares_enabled = config.middleware || [];
    middlewares.forEach(function (middleware) {
        var middleware_name = path.basename(middleware, '.js');
        if(middlewares_enabled.indexOf(middleware_name) !== -1) {
            console.log('Using middleware:', middleware_name);
            var m = require(middleware);
            m.use(app, config);
        }
    });

    var controllers = glob.sync(config.root + '/app/controllers/*.js');
    controllers.forEach(function (controller) {
        require(controller)(app, config);
    });

    app.use(function (req, res, next) {
        var err = new Error('Not Found');
        err.status = 404;
        next(err);
    });

    if(app.get('env') === 'development'){
        app.use(function (err, req, res) {
            res.status(err.status || 500);
            res.render('error', {
                message: err.message,
                error: err,
                title: 'error'
            });
        });
    }

    //TODO: Convert this to a middleware
    app.use(function (err, req, res, next) {
        if(err.name === 'ValidationError') {
            return res.status(400).send(err);
        }

        // console.log(err.stack);
        console.trace(err);

        if(req.path.indexOf(config.api_root) === 0) {
            var message = 'Error [' + (err.status || 500) + ']: ';
            return res.status(err.status || 500).send({message: message + err.message, status: err.status || 500});
        }

        next(err);
    });

    //TODO: Convert this to a middleware
    app.use(function (err, req, res) {
        console.error('#Error:', err);
        res.status(err.status || 500);
        res.render('error', {
            message: err.message,
            error: {},
            title: 'error'
        });
    });

};
