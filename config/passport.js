'use strict';

var User = require('../app/models').User;
var config = require('./config');

var passport = require('passport');
var LocalStrategy = require('passport-local').Strategy;
var FacebookStrategy = require('passport-facebook').Strategy;
var GithubStrategy = require('passport-github').Strategy;
var DropboxOAuth2Strategy = require('passport-dropbox-oauth2').Strategy;


//TODO: Move to config
var GITHUB_CLIENT_ID = '8ce923fe62998419e829';
var GITHUB_CLIENT_SECRET = '0ca29cee10fdfc7319b614f5f6c33b1fd67792fc';

var DROPBOX_CLIENT_ID = 'w1n6wlqqgn70t6n';
var DROPBOX_CLIENT_SECRET = 'ro8p9xmffosd81m';

passport.use(new LocalStrategy( do_local_auth ) );

passport.use(new FacebookStrategy({
    clientID: 'xxxx',//FACEBOOK_APP_ID
    clientSecret: 'xxx',//FACEBOOK_APP_SECRET
    callbackURL: "http://localhost:3000/auth/facebook/callback"
    }, do_facebook_auth
));

passport.use(new GithubStrategy({
    clientID: GITHUB_CLIENT_ID,
    clientSecret: GITHUB_CLIENT_SECRET,
    callbackURL: "http://localhost:3000/auth/github/callback",
    scope: ["user:email", "public_repo"],
    }, do_github_auth
));

passport.use(new DropboxOAuth2Strategy({
    clientID: DROPBOX_CLIENT_ID,
    clientSecret: DROPBOX_CLIENT_SECRET,
    callbackURL: "http://localhost:3000/auth/dropbox/callback",
  }, do_dropbox_auth
));

passport.serializeUser(function(user, done) {
    console.log('Serialize user:', user);
    done(null, user);
});

passport.deserializeUser(function(user, done) {
    console.log('Deserialize user:', user);
    done(null, user);
});

function do_local_auth(username, password, done) {
    User.getAuthenticated(username, password, function(e, user, reason) {
        if (e) { return done(ej); }

        if (!user) {
            var failed_reason = 'Invalid username or password';
            if(reason === User.failedLogin.MAX_ATTEMPTS) {
                failed_reason = 'Your account has been locked due to too many failed attempts';
            }
            return done(null, false, { message: failed_reason });
        }
        return done(null, user.toJSON());
    });
}

function do_facebook_auth(accessToken, refreshToken, profile, done) {
    console.log('accessToken', accessToken);
    console.log('refreshToken', refreshToken);
    console.log('profile', profile);
    done(null, profile);
    /*
    function(accessToken, refreshToken, profile, done) {
    User.findOrCreate(..., function(err, user) {
      if (err) { return done(err); }
      done(null, user);
    });
    } */
}

function do_github_auth(accessToken, refreshToken, profile, done) {
    console.log('accessToken', accessToken);
    console.log('refreshToken', refreshToken);
    console.log('profile', profile);

    // Hack to show name on page
    profile.name = profile.displayName;
    // We want to create user here and link to github info
    done(null, profile);
    /*
    function(accessToken, refreshToken, profile, done) {
    User.findOrCreate(..., function(err, user) {
      if (err) { return done(err); }
      done(null, user);
    });
    } */
}

function do_dropbox_auth(accessToken, refreshToken, profile, done) {
    console.log('accessToken', accessToken);
    console.log('refreshToken', refreshToken);
    console.log('profile', profile);

    // Hack to show name on page
    profile.name = profile.displayName;
    // We want to create user here and link to github info
    done(null, profile);
    /*
    function(accessToken, refreshToken, profile, done) {
    User.findOrCreate(..., function(err, user) {
      if (err) { return done(err); }
      done(null, user);
    });
    } */
}

module.exports = passport;
