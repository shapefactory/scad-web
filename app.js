var express = require('express');
var _ = require('lodash');
var config = require('./config/config');
var glob = require('glob');
var mongoose = require('mongoose');
var models = require('./app/models');

console.log('models', _.keys(models));

mongoose.connect(config.db);
var db = mongoose.connection;
db.on('error', function () {
  throw new Error('unable to connect to database at ' + config.db);
});

db.once('connected', function() {
    console.log('DB Connected');
    var app = express();
    require('./config/express')(app, config);

    console.log('Start server on port', config.port);
    app.listen(config.port);

});

