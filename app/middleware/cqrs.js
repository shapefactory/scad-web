'use strict';

// Enable CQRS for api
module.exports.use = function(app, config) {
    //CORS middleware
    app.use(function(req, res, next) {
        if(req.path.indexOf(config.api_root) === 0) return next();

        res.header('Access-Control-Allow-Origin', config.security.allowedDomains || '*');
        res.header('Access-Control-Allow-Methods', 'GET,PUT,POST,DELETE');
        res.header('Access-Control-Allow-Headers', 'Content-Type,Accept');
        next();
    });
};
