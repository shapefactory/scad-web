'use strict';

/**
 * Authentication nodejs middleware
 *
 * Authenticate requests using basic auth api credentials
 * and API sessions
 *
 */

var _ = require('lodash');
var assert = require('assert');
var models = require('../models');
var ApiToken = models.ApiToken;
var User = models.User;
//var logger = require('../../logger').named('app.middleware.authentication');
//var cache = require('../modules/cache');
//var errors = require('../lib/errorMap.js');

var app, config, newrelic;

/**
 * Entry point for setup middleware
 */
module.exports.use = function(a, c) {
    app = a;
    config = c;

    // check token in config set tokens
    app.use(authenticate_static_token);

    // use user tokens to authenticate
    app.use(authenticate_api_token);

    // a.use(authenticateToken);
    // a.use(authenticateApi);
    // a.use(authenticateSession);
};


function authenticate_static_token(req,res,next) {
    next();
}

function authenticate_api_token(req,res,next) {
    var api_token = extract_token(req);
    if(!api_token) return next();

    ApiToken.findOne({token: api_token}, function(e, token) {
        if(e) return next(e);
        if(!token) return next();

        var query = {_id: token.user_id};
        User.findOne(query, function(e, user) {
            if(e) return next(e);
            if(!user) return next();
            req.user = _.pick(user.toJSON(),  '_id', 'name', 'email', 'roles');
            res.set('X-Auth-Method', 'api-token');
            res.set('X-Auth-Login', user.email);
            next();
        });
    })
}

function authenticate(role) {
    return function(req,res,next) {
        var roles = [];
        if(req.user && req.user.roles) {
            roles = req.user.roles;
        };

        var isAuthenticated = check_role(roles, role);

        console.log('Authentication:', isAuthenticated, roles, role);
        if(isAuthenticated) return next();

        res.status(401).send({message: 'Not authenticated', status: 401});
    };
}

function check_role(roles, role) {
    if(!roles) return false;

    roles = _.map(roles, function(r) {
        return r.toLowerCase();
    });
    role = role.toLowerCase();
    return roles.indexOf(role) !== -1;
};

function extract_token(req) {

    var api_token = false;

    // Get api-key from headers
    //console.log('Headers', req.headers);
    if(req.headers['x-api-token']) {
        api_token = req.headers['x-api-token'];
    }

    // Get api-key from query string
    if(req.query['api-token']) {
        api_token = req.query['api-token'];
    }

    if(req.query['api_token']) {
        api_token = req.query['api_token'];
    }

    // Get api-key from query string
    if(req.body['api-token']) {
        api_token = req.body['api-token'];
    }

    if(req.body['api_token']) {
        api_token = req.body['api_token'];
    }

    return api_token;
}

module.exports.authenticate = authenticate;

module.exports.authenticateApi = authenticateApi;
module.exports.authenticateSession = authenticateSession;
module.exports.authenticateToken = authenticateToken;
module.exports.checkUser = checkUser;
module.exports.checkRescue = checkRescue;
module.exports.checkAdmin = checkAdmin;
module.exports.checkApi = checkApi;
module.exports.checkLoginRole = checkLoginRole;

/**
 * Query the datastore for a session or return a cached result
 *
 * @param String sessionId The session token to search for
 * @param Function Callback with error and result objects
 */
function cachedSession(sessionId, callback) {
    cache.getOrGenerate('sessionCache', sessionId, function(done) {
        sessionService.findByToken(sessionId, function (e, sessionInfo) {
            // Handle NOT_FOUND messages in API
            if (e && e.code !== 301) return done(e);
            done(null, sessionInfo ? ApiSession.toPublic(sessionInfo, true) : null);
        });
    }, function(e, r /*, cached, report*/) {

        callback(e,r);
    });
}

/**
 * Query the datastore or return a cached user
 *
 * @param String The id of the user
 * @param Function Callback with error and result
 */
function cachedUser(userId, callback) {
    cache.getOrGenerate('userCache', userId, function(done) {
        userService.findById(userId, function (e, userInfo) {
            // Handle NOT_FOUND messages in API
            if (e && e.code !== 301) return done(e);

            // FIXME: Error handling. kintel 20140213
            done(null, userInfo);
        });
    }, function(e, r /*, cached, report */) {
        //console.log('cachedUser:', report);
        callback(e, r);
    });
}

function authenticateToken(req, res, next) {
    // Get auth header
    var auth = req.headers['authorization'];
    if(!auth) return next();

    // Only accept Basic auth with an actual auth string
    var parts = auth.split(' ');
    if (parts.length < 2) return errors.sendError(res, errors.BAD_AUTHORIZATION);
    if (parts[0].toLowerCase() !== 'basic') next();

    var credentials = new Buffer(parts[1], 'base64').toString().split(':');
    if (credentials.length !== 2) return errors.sendError(res, errors.BAD_AUTHORIZATION);

    // We don't care about api logins
    if(credentials[0].toLowerCase() === 'api') return next();

    userService.authenticateToken(credentials[0], credentials[1], function (e, result) {
        if (e) return errors.sendError(res, e);

        res.set('X-Auth-Method', 'API-TOKEN');
        res.set('X-Auth-Login', result.login);

        req.user = _.assign(result, {
            isUser: models.User.hasRole(result, 'User'),
            isAdmin: models.User.hasRole(result, 'Admin'),
            isRescue: models.User.hasRole(result, 'Rescue'),
            isRescueUser: models.User.hasRole(result, 'RescueUser'),
        });

        next();
    });
}

// Verify the API credentials in the Authorize header
// if the api credentials are valid assign the user info to req.userapi
// see docs/api-authentication.md for details
function authenticateApi(req, res, next) {

    var apiKey = false;

    // Get api-key from Authorization header
    var auth = req.headers["authorization"];

    // Basic Auth
    if(auth) {
        var parts = auth.split(' ');
        var credentials = new Buffer(parts[1], 'base64').toString().split(':');
        // We're only interested in the 'api' user
        if(credentials[0].toLowerCase() !== 'api') return next();
        apiKey = credentials[1];
    }

    // Get api-key from query string
    if(req.query['api-key']) {
        apiKey = req.query['api-key'];
    }

    if (!apiKey){
        // Used by EnsureLoginApi
        req.apiErrorCode = 109;
        return next();
    }

    var apiKeyInfo = config.security.apiKeys[apiKey];

    if(apiKeyInfo) {
        res.set('X-Auth-Method', 'API-KEY');
        res.set('X-Auth-Login', 'api');

        req.apiUser = { user: 'api', name: apiKeyInfo};
        req.user = {
                id:       'api',
                session:  '',
                name:     'API User',
                email:    'api@i-sea.no',
                login:    'api',
                roles:    ['Api'],
                isUser:   false,
                isAdmin:  true,
                isRescue: false,
        };
    } else {
        // Used by EnsureLoginApi
        req.apiErrorCode = 109;
        req.apiUser = false;
    }

    next();
}

// Verify the api session credentials
// if the session is valid assign the user info to the req.user
function authenticateSession(req, res, next) {
    var apiSessionId = false;

    // We do not care about priority of api session source, if session is defined multiple times
    // the following priority will apply, generally it should not be supplied multiple times
    // with persistent cookies being the exception, this is why cookies are clobbered by query string and
    // headers

    // Get api-key from cookies
    if( req.cookies && req.cookies['api-session']) {
        apiSessionId = req.cookies['api-session'];
    }

    // Get api-key from headers
    //console.log('Headers', req.headers);
    if(req.headers['x-api-session']) {
        apiSessionId = req.headers['x-api-session'];
    }

    // Get api-key from query string
    if(req.query['api-session']) {
        apiSessionId = req.query['api-session'];
    }

    if(!apiSessionId) {
        return next();
    }

    cachedSession(apiSessionId, function(e, sessionInfo) {
        if (e) return errors.sendError(res, e);

        if(!sessionInfo) {
            logger.debug('Session id ' + apiSessionId + ' not found');
            req.apiErrorCode = 102;
            return next();
        }

        // We have a session validate it
        var session = ApiSession.fromPublic(sessionInfo);
        if (ApiSession.validate(session).length > 0) {
            logger.debug('Session id ' + apiSessionId + ' is not valid');
            req.apiErrorCode = 102;
            return next();
        }

        cachedUser(session.user_id, function(e, userInfo) {
            if (e) return errors.sendError(res, e);

            if(!userInfo) {
                logger.debug('Session id ' + apiSessionId + ' user ' + session.user_id + ' is not found');
                req.apiErrorCode = 102;
                return next();
            }

            req.user = models.User.toPublic(userInfo, models.User.hasRole(userInfo, 'Admin'));
            req.user.login = userInfo.login || userInfo.phone;
            req.user.session = session.token;
            req.user.isUser = models.User.hasRole(userInfo, 'User');
            req.user.isAdmin = models.User.hasRole(userInfo, 'Admin');
            req.user.isRescue = models.User.hasRole(userInfo, 'Rescue');
            req.user.isRescueUser = models.User.hasRole(userInfo, 'RescueUser');

            if (newrelic) {
                newrelic.addCustomParameter ('auth_user_id', userInfo.id);
                newrelic.addCustomParameter ('auth_session_id', apiSessionId);
            }

            res.set('X-Auth-Method', 'API-SESSION');
            res.set('X-Auth-Login', userInfo.login);

            next();
        });
    });
}



/*
 * Check if the user has the role or any of the roles if an array is provided
 */
function checkLoginRole(roles, req, res, next) {
    assert(typeof(roles) === 'string' || roles instanceof Array);

    if(typeof roles === 'string') {
        roles = [roles];
    }

    var isValid = req.user && _.reduce(roles, function(val, role) {
        return val || models.User.hasRole(req.user, role);
    }, false);

    //var isValid = req.user && models.User.hasRole(req.user, role);

    if(!isValid) {
        errors.sendError(res, req.user ? errors.ACCESS_DENIED : errors.NOT_AUTHENTICATED);
        return res.end();
    }
    next();
}

function checkApi(req, res, next) {
    checkLoginRole(['Api', 'Admin'], req, res, next);
}

function checkUser(req, res, next) {
    checkLoginRole('User', req, res, next);
}

function checkRescue(req, res, next) {
    checkLoginRole(['Rescue', 'RescueUser'], req, res, next);
}

function checkAdmin(req, res, next) {
    checkLoginRole('Admin', req, res, next);
}


