'use strict';

module.exports.authentication = require('./authentication');
//module.exports.ratelimit = require('./ratelimit');
//module.exports.status = require('./status');
//module.exports.logging = require('./logging');
module.exports.cqrs = require('./cqrs');
//module.exports.requestValidator = require('./requestValidator');
module.exports.localuser = require('./localuser');

