'use strict';

// Enable CQRS for api
module.exports.use = function(app, config) {

    //CORS middleware
    app.use(function(req, res, next) {

        // We don't want this to work for api requests
        if(req.path.indexOf(config.api_root) === 0) return next();
        if(req.user) {
            // res.locals = req.locals || {};
            res.locals.user = req.user;
        } else {
            // res.locals = req.locals || {};
            res.locals.user = null;
        }
        next();
    });

};
