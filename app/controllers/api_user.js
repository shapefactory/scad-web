'use strict';

var _ = require('lodash');
var express = require('express');
var router = express.Router();
var User = require('../models').User;

var config;

module.exports = function (app,c) {
    config = c;
    app.use(config.api_root, router);
};

module.exports.list_users = list_users;

router.get('/users', list_users);
router.post('/users', create_user);
router.post('/users/check', check_email);
router.post('/users/register', create_user);

function list_users(req, res, next) {
    User.find(function (err, users) {
        if (err) return next(err);
        res.send(users);
    });
}

function create_user(req, res, next) {
    var user = _.pick(req.body, 'name', 'email', 'password');
    // console.log('User:', user);
    User.create(user, function(e, created_user) {
        if (e) return next(e);
        created_user = created_user.toJSON();
        delete created_user.password;
        res.status(201).send(created_user);
    });
}

function check_email(req, res, next) {
    ///TODO: Move to general validation handing
    if(!req.body.email) return res.status(400).send({name: 'ValidationError', message: 'Expected body field: email'});
    User.count({email: req.body.email}, function(e, count) {
        if (e) return next(e);
        res.status(200).send({ available: count === 0 ? 'yes': 'no'});
    });
}

