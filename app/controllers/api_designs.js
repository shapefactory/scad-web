'use strict';

var _ = require('lodash');
var express = require('express');
var router = express.Router();
var Design = require('../models').Design;

var config;

module.exports = function (app,c) {
    config = c;
    app.use(config.api_root, router);
};

router.get('/designs/seed_data', seed_data);
router.get('/designs', list_designs);
router.post('/designs', create_design);


var designs = [
    {
        "id": "001",
        "title": "test 1",
        "image": "test1.png",
        "ref": "test1.scad",
        "tagline": "blah blah #1",
        "description": "blah blah #1",
        "lastmodified": 1424202876
    },
    {
        "id": "002",
        "title": "test 2",
        "image": "test2.png",
        "ref": "test2.scad",
        "tagline": "blah blah #2",
        "description": "blah blah #2",
        "lastmodified": 1424202881
    },
    {
        "id": "003",
        "title": "test 3",
        "image": "test3.png",
        "ref": "test3.scad",
        "tagline": "blah blah #3",
        "description": "blah blah #3",
        "lastmodified": 1424202890
    }
];

function list_designs(req, res, next) {
    ///TODO: Wire with database model
    res.status(200).send(designs);

    //User.find(function (err, users) {
    //    if (err) return next(err);
    //    res.send(users);
    //});
}

function create_design(req,res,next) {
    ///TODO: Implement logic and validation
    var design = req.body;
    designs.push(design);
    res.status(201).send({message:'ok'});
}

function seed_data(req,res,next) {

}
