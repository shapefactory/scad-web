'use strict';

var express = require('express');
var router = express.Router();
var User = require('../models').User;
var passport = require('../../config/passport');

var config;

module.exports = function (app,c) {
    config = c;
    app.use('/', router);
};

router.get('/login', login);
router.get('/logout', logout);

var passport_opt = {
    successRedirect: '/',
    failureRedirect: '/login',
    failureFlash: true
};

router.post('/auth/local', passport.authenticate('local', passport_opt));
router.get( '/auth/facebook', passport.authenticate('facebook', passport_opt));
router.post('/auth/facebook', passport.authenticate('facebook', passport_opt));
router.get( '/auth/github/callback', passport.authenticate('github', passport_opt), do_login_facebook);

router.get( '/auth/github', passport.authenticate('github', passport_opt));
router.post('/auth/github', passport.authenticate('github', passport_opt));
router.get( '/auth/github/callback', passport.authenticate('github', passport_opt), do_login_github);

router.get( '/auth/dropbox', passport.authenticate('dropbox-oauth2', passport_opt));
router.post('/auth/dropbox', passport.authenticate('dropbox-oauth2', passport_opt));
router.get( '/auth/dropbox/callback', passport.authenticate('dropbox-oauth2', passport_opt), do_login_dropbox);


function logout(req,res,next) {
    req.logout();
    res.redirect('/');
}

function login(req,res,next) {
    console.log("session %j", req.session);

    res.render('login', {
        title: 'Login',
        error: req.flash('error'),
    });
}

function do_login(req,res,next) {

    var username = req.body.username || '';
    var password = req.body.password || '';

    User.getAuthenticated(username, password, function(e, user, reason) {
        console.log('User:', user);
        console.log('Reason:', reason);

        reason = 'Invalid username or password';
        req.flash('message', 'Login failed: ' + reason);
        res.redirect('/login');

    });

}


function do_login_facebook(req,res,next) {
    console.log('Facebook login');

    // Logged in
    res.redirect('/');
}

function do_login_github(req,res,next) {
    console.log('Githib login');

    // Logged in
    res.redirect('/');
}

function do_login_dropbox(req,res,next) {
    console.log('Dropbox login');

    // Logged in
    res.redirect('/');
}
