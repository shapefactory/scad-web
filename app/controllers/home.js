'use strict';

var express = require('express');
var router = express.Router();

var config;

module.exports = function (app, c) {
  app.use('/', router);
  config = c;
};

router.get('/', function (req, res, next) {
    console.log("passport user", req.user);
    console.log("session", JSON.stringify(req.session));
    res.render('index', {
        title: 'Home',
        config: config,
    });
});

router.get('/about', function (req, res, next) {
    console.log("passport user", req.user);
    console.log("session", JSON.stringify(req.session));
    res.render('index', {
        title: 'About',
        config: config,
    });
});

router.get('/contact', function (req, res, next) {
    console.log("passport user", req.user);
    console.log("session", JSON.stringify(req.session));
    res.render('index', {
        title: 'Contact',
        config: config,
    });
});

router.get('/customizer', function (req, res, next) {
    console.log("passport user", req.user);
    console.log("session", JSON.stringify(req.session));
    res.render('customizer', {
        title: 'Customizer',
        config: config,
    });
});
