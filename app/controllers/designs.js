'use strict';

var express = require('express');
var router = express.Router();

var config;

module.exports = function (app, c) {
  app.use('/', router);
  config = c;
};

router.get('/designs', function (req, res, next) {
    console.log("passport user", req.user);
    console.log("session", JSON.stringify(req.session));
    res.render('designs', {
        title: 'Designs',
        config: config,
    });
});

router.post('/tools/:type', function (req, res, next) {

var type = req.params.type;
	if(type == "png" || type=="stl"){
	//	res.send('PNG');

		var cmd = req.body.cmd;

		console.log(cmd);

		var sys = require('sys')
		var exec = require('child_process').exec;
		var child;

		child = exec(cmd, function (error, stdout, stderr) {
		  sys.print('stdout: ' + stdout);
		  sys.print('stderr: ' + stderr);
		  if (error !== null) {
		    console.log('exec error: ' + error);
		  }
			res.send(type);

		});
	}

});

router.get('/designs/:id', function (req, res, next) {

	var id = req.params.id;

		console.log("ID:" + id);
		res.render('design', {
        title: 'Design',
        config: config,
				id: id
    });

});




// router.get('*', function(req, res) {
//   notFound(res);
// });
// function notFound(res)
// {
//   res.send('<h1>Page not found.</h1>', 404);
// }
