'use strict';

var _ = require('lodash');
var express = require('express');
var router = express.Router();
var fs = require("fs");

var config;

module.exports = function (app,c) {
    config = c;
    app.use(config.api_root, router);
};

router.post('/tasks/scad_to_png', generate_png);

function generate_png(req, res, next) {
    ///TODO: Implement backend task queue

    var image_origial = "public/inc/files/out.png";
    fs.readFile(image_origial, function(err, original_data) {
        console.log(typeof original_data);
        var base64_data = new Buffer(original_data, 'binary').toString('base64');

        var result = {
            content_type: 'image/png',
            content_body: base64_data,
            image_width: 512,
            image_height: 512,
            task_type: 'scad_to_png'
        };
        res.status(200).send(result);
    });

}
