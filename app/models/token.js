'use strict';

var mongoose = require('mongoose');
var Schema = mongoose.Schema;
var uuid = require('node-uuid');
var ms = require('ms');

// var TOKEN_EXPIRY = '1y';
// var TOKEN_EXPIRY = '1d';
var TOKEN_EXPIRY = '5m';

var dt_expiry = function() {
    return Date.now() + ms(TOKEN_EXPIRY);
}

var TokenSchema = new Schema({
    token: {type:String, index: true, default: uuid.v4 },
    app: {type:String},
    description: {type:String},
    created_at: { type: Date, default: Date.now },
    expires_at: { type: Date, expires: 0, default: dt_expiry },
    user_id: Schema.ObjectId,
});

TokenSchema.set('autoIndex', true);

module.exports = mongoose.model('ApiToken', TokenSchema);
module.exports.schema = TokenSchema;
