'use strict';

var glob = require('glob');
var config = require('../../config/config');
var models = glob.sync(config.root + '/app/models/*.js');

models.forEach(function (model) {
    if(model.indexOf('index.js') != -1) return;
    var m = require(model);
    console.log('Adding model:', m.modelName, '[', model, ']');
    module.exports[m.modelName] = m;
});
