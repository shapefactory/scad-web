'use strict';

var mongoose = require('mongoose');
var Schema = mongoose.Schema;
var bcrypt = require('bcrypt');
var SALT_WORK_FACTOR = 10;
var MAX_LOGIN_ATTEMPTS = 5;
// var LOCK_TIME = 1 * 60 * 60 * 1000;
var LOCK_TIME = 60 * 1000;

var ApiToken = require('./token');

// var passportLocalMongoose = require('passport-local-mongoose');

//http://stackoverflow.com/questions/14588032/mongoose-password-hashing
//http://blog.mongodb.org/post/32866457221/password-authentication-with-mongoose-part-1

var UserSchema = new Schema({
    licence: String,
    name: String,
    email: {type: String, required: true, unique : true, index: {unique: true} },
    password: {type: String, required: true, select: true },
    login_attempts: { type: Number, required: true, default: 0 },
    lock_until: { type: Number },
    created_at: { type: Date, default: Date.now },
    roles: { type: [String], default: ['User']},
    //apiTokens: { type: [ApiToken.schema], default: [] },

    twitter_id: { type: String, index: true},
    facebook_id: { type: String, index: true},
    github_id: { type: String, index: true},
    github_login: { type: String, index: true},

    account_links: {
        github: {},
        facebook: {},
        twitter: {},
    },

});

// UserSchema.plugin(passportLocalMongoose, {usernameField: 'email', saltField: 'salt', hashField: 'hashField', usernameLowerCase: true});

UserSchema.virtual('isLocked').get(function() {
    // check for a future lockUntil timestamp
    return !!(this.lock_until && this.lock_until > Date.now());
});

UserSchema.virtual('date')
.get(function(){
    return this._id.getTimestamp();
});

UserSchema.path('email').validate(function(v, fn) {
    var user = this;
    console.log('User: ', user);
  // Make sure the email address is not already registered
    mongoose.model('User').find({'email': v.toLowerCase()}, function (err, emails) {
        fn(err || emails.length === 0);
    });
}, 'Email must be unique');

UserSchema.path('email').validate(function (email) {
    var emailRegex = /^([\w-\.]+@([\w-]+\.)+[\w-]{2,4})?$/;
    return emailRegex.test(email);
}, 'The e-mail field cannot be empty and must be a valid email.');

UserSchema.pre('save', function(next) {
    var user = this;

    // only hash the password if it has been modified (or is new)
    if (!user.isModified('password')) return next();

    // generate a salt
    bcrypt.genSalt(SALT_WORK_FACTOR, function(err, salt) {
        if (err) return next(err);

        // hash the password using our new salt
        bcrypt.hash(user.password, salt, function(err, hash) {
            if (err) return next(err);

            // override the clear-text password with the hashed one
            user.password = hash;
            next();
        });
    });
});

UserSchema.methods.comparePassword = function(candidatePassword, cb) {
    bcrypt.compare(candidatePassword, this.password, function(err, isMatch) {
        if (err) return cb(err);
        cb(null, isMatch);
    });
};

UserSchema.methods.incLoginAttempts = function(cb) {
    // if we have a previous lock that has expired, restart at 1
    if (this.lock_until && this.lock_until < Date.now()) {
        return this.update({
            $set: { login_attempts: 1 },
            $unset: { lock_until: 1 }
        }, cb);
    }
    // otherwise we're incrementing
    var updates = { $inc: { login_attempts: 1 } };
    // lock the account if we've reached max attempts and it's not locked already
    if (this.login_attempts + 1 >= MAX_LOGIN_ATTEMPTS && !this.isLocked) {
        updates.$set = { lock_until: Date.now() + LOCK_TIME };
    }
    return this.update(updates, cb);
};

// expose enum on the model, and provide an internal convenience reference
var reasons = UserSchema.statics.failedLogin = {
    NOT_FOUND: 0,
    PASSWORD_INCORRECT: 1,
    MAX_ATTEMPTS: 2
};

UserSchema.statics.getAuthenticated = function(username, password, cb) {
    this.findOne({ email: username }, function(err, user) {
        if (err) return cb(err);

        // make sure the user exists
        if (!user) {
            return cb(null, null, reasons.NOT_FOUND);
        }

        // check if the account is currently locked
        if (user.isLocked) {
            // just increment login attempts if account is already locked
            return user.incLoginAttempts(function(err) {
                if (err) return cb(err);
                return cb(null, null, reasons.MAX_ATTEMPTS);
            });
        }

        // test for a matching password
        user.comparePassword(password, function(err, isMatch) {
            if (err) return cb(err);

            // check if the password was a match
            if (isMatch) {
                // if there's no lock or failed attempts, just return the user
                if (!user.login_attempts && !user.lock_until) return cb(null, user);

                // reset attempts and lock info
                var updates = {
                    $set: { login_attempts: 0 },
                    $unset: { lock_until: 1 }
                };

                return user.update(updates, function(err) {
                    if (err) return cb(err);
                    return cb(null, user);
                });
            }

            // password is incorrect, so increment login attempts before responding
            user.incLoginAttempts(function(err) {
                if (err) return cb(err);
                return cb(null, null, reasons.PASSWORD_INCORRECT);
            });
        });
    });
};

UserSchema.methods.createToken = function(app_name, cb) {
    var token = new ApiToken({ app: app_name, description: 'Created token for ' + app_name });
    token.user_id = this._id;
    token.app = app_name;

    console.log('Saving token:', token);

    token.save(function(e, saved_token) {
        if (e) return cb(e);
        cb(null, saved_token);
    });
};

module.exports = mongoose.model('User', UserSchema);
module.exports.schema = UserSchema;

