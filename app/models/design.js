'use strict';

var mongoose = require('mongoose');
var Schema = mongoose.Schema;
var uuid = require('node-uuid');


var StatsSchema = new Schema({
    views: {type:Number},
    downloads: {type:Number},
});

var DesignSchema = new Schema({
    user_id: Schema.ObjectId,
    type: {type: String}, //TODO: Is this a string

    files: {type: [String]}, //TODO: Is this a string
    primary_file: { type: String},
    description: {type: String},
    //links to deps ???
    thumbnail_url: {type: String},
    images: {type: [String]},
    links: {type: [String]},// We need some state for syncing when and when was last sync attempt.
    synk_links: {type: Boolean},
    licence: {type: String},

    tags: {type: [String]},
    //Soccial media stuff likes ratings ???
    stats: { type: { views: Number, downloads: Number } },
});

DesignSchema.set('autoIndex', true);

module.exports = mongoose.model('Design', DesignSchema);
module.exports.schema = DesignSchema;
