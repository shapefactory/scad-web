Array.prototype.clean = function(deleteValue) {
  for (var i = 0; i < this.length; i++) {
    if (this[i] == deleteValue) {         
      this.splice(i, 1);
      i--;
    }
  }
  return this;
};

var app = angular.module("customizerApp", ['ui.utils']);

app.controller("customizerCtrl", function($scope, $http, $parse) {
	$scope.code = "test1.scad";
	
  $scope.submit = function() {
		var scad;
		$http.get($scope.code)
		.success(function(data, status, headers, config) {
			scad = data + "\n\n" + "/*CUSTOMIZED DATA*/";
			angular.forEach($scope.customizerVar, function(value, key){
				var customized_data = key + " = " + value + ";";
				scad = scad + "\n" + customized_data;
			});
			
//			console.log(scad);

			$http.post('/node/other', {type:'download',id:0})
			.success(function(data, status, headers, config){
				console.log(data);
			});

			/*
			$http.post('/node/download', {'filename':$scope.code, 'scad':scad})
			.success(function(data, status, headers, config){
				
				console.log(data);
				
			//console.log(data);
			//success callback
			});
			*/

		});
		
	};
	
	
	$scope.$watch("code", function(newValue, oldValue) {
		
	if ($scope.code.length > 0) {

	  $scope.customizerVar = {};

		$http.get($scope.code)
		.success(function(data, status, headers, config) {
			var data_array=Array(); var data_string="";

			data = data.split("\n");
			data = data.clean("");
			data = data.reverse();
			for (i = 0; i < data.length; i++) {
				data[i] = data[i].trim();
				errMsg = "";
				try{
					eval(data[i]);
				}
				catch(err) {
					errMsg = err.message;
				}
			
				if(errMsg=="") { data_array[i] = data[i]; }
				else { data[i]=""; data_array[i] = data[i]; }				
			}

			data_array = data_array.clean("");
			//remove variable if it contains an operator
			for (i = 0; i < data_array.length; i++) {
				data_array[i] = data_array[i].replace(/"/g, "");
				data_array[i] = data_array[i].replace("[", "");
				data_array[i] = data_array[i].replace("]", "");
				if(data_array[i].indexOf('//') == -1 && data_array[i].indexOf('/*') == -1){
					if(data_array[i].indexOf('+') >=0) { 
						data_array[i]="";
					} else if(data_array[i].indexOf('-') >= 0) {
						data_array[i]="";
					} else if(data_array[i].indexOf('*') >= 0) {
						data_array[i]="";
					} else if(data_array[i].indexOf('/') >= 0) {
						data_array[i]="";
					}
				}
			}

			var data_vars = Array();
		
			data_array = data_array.reverse();
			data_array = data_array.clean("");
			//create string from array
			for (i = 0; i < data_array.length; i++) {
				if(data_array[i].substr(0,2)!="//" && data_array[i].substr(0,2)!="/*"){
					if(data_array[i-1]){
						if(data_array[i-1].substr(0,2)=="//"){
							if(data_array[i].indexOf('//') == -1) data_vars.push(data_array[i] + "//" + "null " + data_array[i-1]);
							else data_vars.push(data_array[i] + data_array[i-1]);
						} else {
							if(data_array[i].indexOf('//') == -1) data_vars.push(data_array[i] + "//" + "null" + "//" + "null");
							else data_vars.push(data_array[i] + "//" + "null");
						}
					} else {
						if(data_array[i].indexOf('//') == -1) data_vars.push(data_array[i] + "//" + "null" + "//" + "null");
						else data_vars.push(data_array[i] + "//" + "null");
					}
				}						
				data_string += data_array[i] + "\n";
			}
		
			for (i = 0; i < data_vars.length; i++) {
				data_vars[i] = data_vars[i].trim();
				data_vars[i] = data_vars[i].replace(';','');
				data_vars[i] = data_vars[i].split('//');
				data_vars[i][0] = data_vars[i][0].split('=');
				data_vars[i][1] = data_vars[i][1].replace(" ","");
				data_vars[i][1] = data_vars[i][1].split(',');

//				console.log(data_vars[i][0][1]);

				$scope.customizerVar[data_vars[i][0][0].trim()] = data_vars[i][0][1].trim();

				if(data_vars[i][1].length==1) {
					if(data_vars[i][1][0].indexOf(':') >= 0) {
						data_vars[i][3]="range";
//						$scope.range = parseInt(data_vars[i][0][1]);
					} else data_vars[i][3]="text";
				} else {
					data_vars[i][3]="select";
				}
			
				//console.log(data_vars[i][1].length);
			
				if(data_vars[i][3]!="text"){
					for(j=0; j < data_vars[i][1].length; j++){
						if(data_vars[i][1][j].indexOf(':') == -1) data_vars[i][1][j]+=":"+data_vars[i][1][j];
						data_vars[i][1][j] = data_vars[i][1][j].split(":");
					}
				}
			
				data_vars[i][2] = data_vars[i][2].replace(" ","");
				data_vars[i][0][0] = data_vars[i][0][0].trim();
				data_vars[i][0][1] = data_vars[i][0][1].trim();
			}
		
			data_json = JSON.parse(JSON.stringify(data_vars));

			$scope.data_array = data_array;
			$scope.data_string = data_string;
			$scope.data_vars = data_vars;
			$scope.data_json = data_json;
			$scope.inflectorType = 'humanize';

		});
	}
	});

});