Array.prototype.clean = function(deleteValue) {
  for (var i = 0; i < this.length; i++) {
    if (this[i] == deleteValue) {         
      this.splice(i, 1);
      i--;
    }
  }
  return this;
};



var app = angular.module("customizerApp", ['ui.utils', 'ngAnimate', 'ui.bootstrap']);

app.controller("customizerCtrl", ['$scope', '$http', '$parse', '$timeout', '$location', function($scope, $http, $parse, $timeout, $location) {
	
	$scope.id = $location.absUrl().replace('http://localhost:3000/designs/','');

	$scope.showPreview = 1;
		
	$scope.design = "";
	
  $scope.previewAngle = 'default';
	
	var designs_json = "/inc/designs.json";

	$http.get(designs_json)
	.success(function(data, status, headers, config) {

		$scope.designs = data.designs;
		
		for (i = 0; i < $scope.designs.length; i++) { 
//			$scope.designs[i].imageurl = "/inc/files/" + $scope.designs[i].image;
			if($scope.designs[i].id == $scope.id) { $scope.design = $scope.designs[i]; }
		}

		$scope.design.imageurl = "/inc/files/" + $scope.design.image;
		
		$scope.ref = $scope.design.ref;
	
		$scope.imageurl = "";
	
//		$scope.$watch("data_json", function(newValue, oldValue) {

			$scope.refurl = "/inc/files/"+$scope.ref;
		
			if ($scope.ref.length > 0) {

			  $scope.customizerVar = {};

				$http.get($scope.refurl)
				.success(function(data, status, headers, config) {
					var data_array=Array(); var data_string="";

					data = data.split("\n");
					data = data.clean("");
					//data = data.reverse();
					for (i = 0; i < data.length; i++) {
						data[i] = data[i].trim();
						/*stop evaluating if module GoAwayCustomizer() is included*/
						if(data[i]=="module GoAwayCustomizer() {") break;
						errMsg = "";
						try{
							eval(data[i]);
						}
						catch(err) {
							errMsg = err.message;
						}
						/*fix for if a variable begins with a number*/
						if(errMsg == "Unexpected token ILLEGAL" && !isNaN(data[i][0])){
							data[i] = "___"+data[i];
							errMsg = "";
							try{
								eval(data[i]);
							}
							catch(err) {
								errMsg = err.message;
							}
						}
						if(errMsg=="") { data_array[i] = data[i]; }
						else { data[i]=""; data_array[i] = data[i]; }
					}
					data_array = data_array.reverse();
					data_array = data_array.clean("");
					
//					console.log(data_array);
					//remove variable if it contains an operator
					for (i = 0; i < data_array.length; i++) {
						data_array[i] = data_array[i].replace(/"/g, "");
						data_array[i] = data_array[i].replace("[", "");
						data_array[i] = data_array[i].replace("]", "");
						if(data_array[i].indexOf('//') == -1 && data_array[i].indexOf('/*') == -1){
							if(data_array[i].indexOf('+') >=0) { 
								data_array[i]="";
							} else if(data_array[i].indexOf('-') >= 0) {
								data_array[i]="";
							} else if(data_array[i].indexOf('*') >= 0) {
								data_array[i]="";
							} else if(data_array[i].indexOf('/') >= 0) {
								data_array[i]="";
							}
						}
					}

					var data_vars = Array();
		
					data_array = data_array.reverse();
					data_array = data_array.clean("");
					//create string from array
					for (i = 0; i < data_array.length; i++) {
						if(data_array[i].substr(0,2)!="//" && data_array[i].substr(0,2)!="/*"){
							if(data_array[i-1]){
								if(data_array[i-1].substr(0,2)=="//"){
									if(data_array[i].indexOf('//') == -1) data_vars.push(data_array[i] + "//" + "null " + data_array[i-1]);
									else data_vars.push(data_array[i] + data_array[i-1]);
								} else {
									if(data_array[i].indexOf('//') == -1) data_vars.push(data_array[i] + "//" + "null" + "//" + "null");
									else data_vars.push(data_array[i] + "//" + "null");
								}
							} else {
								if(data_array[i].indexOf('//') == -1) data_vars.push(data_array[i] + "//" + "null" + "//" + "null");
								else data_vars.push(data_array[i] + "//" + "null");
							}
						}						
						data_string += data_array[i] + "\n";
					}
		
					for (i = 0; i < data_vars.length; i++) {
						data_vars[i] = data_vars[i].trim();
						data_vars[i] = data_vars[i].replace(';','');
						data_vars[i] = data_vars[i].split('//');
						data_vars[i][0] = data_vars[i][0].split('=');
						data_vars[i][1] = data_vars[i][1].replace(" ","");
						data_vars[i][1] = data_vars[i][1].split(',');

		//				console.log(data_vars[i][0][1]);

						$scope.customizerVar[data_vars[i][0][0].trim()] = data_vars[i][0][1].trim();

						if(data_vars[i][1].length==1) {
							if(data_vars[i][1][0].indexOf(':') >= 0) {
								data_vars[i][3]="range";
		//						$scope.range = parseInt(data_vars[i][0][1]);
							} else data_vars[i][3]="text";
						} else {
							data_vars[i][3]="select";
						}
						
						data_vars[i][0][0] = data_vars[i][0][0].trim();
						data_vars[i][0][1] = data_vars[i][0][1].trim();
						data_vars[i][2] = data_vars[i][2].trim();

						if(data_vars[i][1]!="null"){
							for (j = 0; j < data_vars[i][1].length; j++) {
								data_vars[i][1][j] = data_vars[i][1][j].trim();
//								console.log(data_vars[i][1][j]);
							}
						}
						//console.log(data_vars[i][1].length);
			
						if(data_vars[i][3]!="text"){
							for(j=0; j < data_vars[i][1].length; j++){
								if(data_vars[i][1][j].indexOf(':') == -1) data_vars[i][1][j]+=":"+data_vars[i][1][j];
								data_vars[i][1][j] = data_vars[i][1][j].split(":");
							}
						}
//						console.log(data_vars[i]);
//						data_vars[i][2] = data_vars[i][2].replace(" ","");
						data_vars[i][0][0] = data_vars[i][0][0].trim();
						data_vars[i][0][1] = data_vars[i][0][1].trim();



						if(data_vars[i][3]=="text"){
//							console.log(parseFloat(data_vars[i][0][1]));
							if(!isNaN(parseFloat(data_vars[i][0][1]))){
								data_vars[i][0][1] = parseFloat(data_vars[i][0][1]);
								data_vars[i][3] = "number";
							}
						}						

					}
	
		
					data_json = JSON.parse(JSON.stringify(data_vars));

					$scope.data_array = data_array;
					$scope.data_string = data_string;
					$scope.data_vars = data_vars;
					$scope.data_json = data_json;
					$scope.inflectorType = 'humanize';
				});
			}
//		});
	
	
	});

	function getCmd(){
		var customized_trimmed = "";
		angular.forEach($scope.customizerVar, function(value, key){
			if(isNaN(value)) value = '"'+value.trim()+'"';
			/*remove _ from variables beginning with number*/
			if(key.substr(0,3)=="___") key = key.substr(3);
			customized_trimmed = customized_trimmed.trim() + key + " = " + value + ";";
		});
		var cmd = "openscad public" + $scope.refurl + " -D'"+customized_trimmed+"' --autocenter --viewall "+$scope.camera+" -o public/png/out_"+$scope.id+".png";
		return cmd;
	}
	
	function getPng(){
		var cmd = getCmd();
		$http.post('/tools/png', {cmd: cmd})
		.success(function(data, status, headers, config){
			if(data=="png") { 
				$scope.showPreview = 0;
				$scope.design.imageurl = "../img/blank.png";
				$timeout(function(){
					$scope.showPreview = 1;
					$scope.design.imageurl = "../png/out_"+$scope.id+".png";
				},100);
			}
			else {
			}
		});
	}

	$scope.change = function(){
		if($scope.previewAngle=="right") { $scope.camera = "--camera=0,0,0,90,0,90,140"; }
		else if($scope.previewAngle=="top") { $scope.camera = "--camera=0,0,0,0,0,0,140"; }
		else if($scope.previewAngle=="bottom") { $scope.camera = "--camera=0,0,0,180,0,0,140"; }
		else if($scope.previewAngle=="left") { $scope.camera = "--camera=0,0,0,90,0,270,140"; }
		else if($scope.previewAngle=="front") { $scope.camera = "--camera=0,0,0,90,0,0,140"; }
		else if($scope.previewAngle=="back") { $scope.camera = "--camera=0,0,0,90,0,180,140"; }
		else { $scope.camera = ""; }

		getPng();
	}
		
  $scope.submit = function() {
		var scad, customized_data = "", customized_trimmed = "";
//		$http.get($scope.refurl)
//		.success(function(data, status, headers, config) {
//			scad = data + "\n\n" + "/*CUSTOMIZED DATA*/";
			angular.forEach($scope.customizerVar, function(value, key){
				if(isNaN(value)) value = '"'+value.trim()+'"';
				/*remove _ from variables beginning with number*/
				if(key.substr(0,3)=="___") key = key.substr(3);
				customized_trimmed = customized_trimmed.trim() + key + " = " + value + ";";
			});

//			console.log(customized_data);

			scad = scad + customized_data;
			var cmd = "openscad public" + $scope.refurl + " -D'"+customized_trimmed+"' -o public/stl/out_"+$scope.id+".stl";

			$http.post('/tools/stl', { cmd: cmd })
			.success(function(data, status, headers, config){
//				console.log(data);
				if(data=="stl") { 
					window.location.href="/stl/out_"+$scope.id+".stl";
				}
			});
			
			//});
		
	}

}]);