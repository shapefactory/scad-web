Array.prototype.clean = function(deleteValue) {
  for (var i = 0; i < this.length; i++) {
    if (this[i] == deleteValue) {         
      this.splice(i, 1);
      i--;
    }
  }
  return this;
};

var app = angular.module("customizerApp", ['ui.utils']);

app.controller("customizerCtrl", function($scope, $http, $parse) {
	
	var designs_json = "/inc/designs.json";

	$http.get(designs_json)
	.success(function(data, status, headers, config) {

		$scope.designs = data.designs;
		
		for (i = 0; i < $scope.designs.length; i++) { 
			$scope.designs[i].imageurl = "/inc/files/" + $scope.designs[i].image;
		}

	});

	$scope.code = "";

});